package hotel;

/**
 * Created by Patryk on 2016-03-10.
 */
public interface RoomStateMachine {

    public abstract void hireRoom();
    public abstract void freeRoom();
}
