package hotel;

/**
 * Created by Patryk on 2016-03-09.
 */
public class HiredRoom extends Room implements RoomState {

    public HiredRoom(int costPerDay, int numberOfBeds, int roomNumber, boolean hasWC, boolean babyExtensionAvailable) {
        super(costPerDay, numberOfBeds, roomNumber, hasWC, babyExtensionAvailable);
    }

    @Override
    public void rent(Guest guest) {
        {
            System.out.println("ROOM IS ALREADY HIRED by: "+ this.currentGuest.getName());
        }
    }

    @Override
    public void free() {

    }

    @Override
    public void printIsFree() {
        System.out.println("ROOM IS HIRED");
    }


}
