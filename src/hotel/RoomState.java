package hotel;

/**
 * Created by Patryk on 2016-03-09.
 */
public interface RoomState {
    public void rent(Guest guest);
    public void free();
    public void printIsFree();
}
