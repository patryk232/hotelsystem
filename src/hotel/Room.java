package hotel;

/**
 * Created by Patryk on 2016-03-08.
 */
public abstract class Room implements RoomState{
    protected final int costPerDay;
    protected final int numberOfBeds;
    protected final int roomNumber;
    protected boolean isFree = true;
    protected final boolean hasWC;
    protected final boolean babyExtensionAvailable;
    protected Guest currentGuest  = null;

    public Room(int costPerDay, int numberOfBeds, int roomNumber, boolean hasWC, boolean babyExtensionAvailable) {
        this.costPerDay = costPerDay;
        this.numberOfBeds = numberOfBeds;
        this.roomNumber = roomNumber;
        this.hasWC = hasWC;
        this.babyExtensionAvailable = babyExtensionAvailable;
    }

    public void print() {
        System.out.println("Room: " + roomNumber );
        System.out.println("Cost per day: "+ costPerDay);
        System.out.println("Number of beds: " + numberOfBeds);
        printHasWc();
        printIsFree();
        System.out.println();
    }

    public void printHasWc() {
        if (hasWC)
        {
            System.out.println("Has WC ");
        } else {
            System.out.println("Hasn't a WC ");
        }
    }

}
