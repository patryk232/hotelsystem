package hotel;


import java.util.ArrayList;

import java.util.Scanner;

/**
 * Created by Patryk on 2016-03-09.
 */
public class GuestsBase {
    ArrayList<Guest> guestsList = new ArrayList<>();

    public void addGuestToBase() {
        Scanner input = new Scanner(System.in);
        System.out.println("Write Guest's name and surname: ");
        String name = input.nextLine();
        Guest guest = new Guest(name);
        guestsList.add(guest);
    }

    public void showGuestList() {
        for (Guest guest : guestsList) {
            System.out.print(guestsList.indexOf(guest) + ". ");
            guest.print();
        }

    }
}
