package hotel;

/**
 * Created by Patryk on 2016-03-09.
 */
public class FreeRoom extends Room implements RoomState {

    public FreeRoom(int costPerDay, int numberOfBeds, int roomNumber, boolean hasWC, boolean babyExtensionAvailable) {
        super(costPerDay, numberOfBeds, roomNumber, hasWC, babyExtensionAvailable);
    }

    @Override
    public void rent(Guest guest) {
        {
            currentGuest = guest;
        }
    }

    @Override
    public void free() {

    }

    @Override
    public void printIsFree() {
    System.out.println("ROOM IS FREE");
    }


}
