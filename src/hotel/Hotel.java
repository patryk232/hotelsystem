package hotel;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.Scanner;

/**
 * Created by Patryk on 2016-03-08.
 */
public class Hotel  {
    private static boolean quitFlag = false;

    GuestsBase guestsBase = new GuestsBase();

    public static void main(String[] args) {
        {
            //JPA INITIATE

             EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("newBase");
             EntityManager entityManager = entityManagerFactory.createEntityManager();

            entityManager.close();
            entityManagerFactory.close();

            //JPA INITIATE

            ListOfRooms listOfRooms = new ListOfRooms();
            Scanner input = new Scanner(System.in);
            GuestsBase guestsBase = new GuestsBase();
            BookOfReservation bookOfReservation = new BookOfReservation();

            guestsBase.addGuestToBase();
            Hotel hotel = new Hotel();
            for (int i = 0; i < 10; i++) {
                Room room=new FreeRoom(100+i*10,2,i+1,true,true) ;
                listOfRooms.roomList.add(room);
            }
            while(!quitFlag)
            {

                System.out.println("* * * * * * * * * *\n Obsługa HOTELU \n");
                System.out.println("  Wybierz interesującą Cię opcję: \n");
                System.out.println("  1. Dodaj gościa do bazy \n");
                System.out.println("  2. Zarezerwuj pokój \n");
                System.out.println("  3. Przeglądaj pokoje \n");
                System.out.println("  4. Wyszukaj wolne pokoje \n");
                System.out.println("  5. Edytuj istniejącą rezerwację \n");
                System.out.println("  6. Show Guests list \n");
                System.out.println("  7. Zakończ \n * * * * * * * * * *\n");
                int choose = input.nextInt();
                switch(choose) {
                    case 1:{
                        guestsBase.addGuestToBase();
                        break;
                    }
                    case 2:{
                        //TODO
                        clearScr();
                        break;
                    }
                    case 3:{
                        listOfRooms.showRoomList();
                      //  clearScr();
                        break;
                    }
                    case 4:{
                        //TODO
                        clearScr();
                        break;
                    }
                    case 5:{
                        //TODO
                        clearScr();
                        break;
                    }
                    case 6:{
                        guestsBase.showGuestList();
                        //clearScr();
                        break;
                    }
                    case 7: {
                        clearScr();
                        quitFlag = true;
                    }
                    default:
                }
            }
        }
    }
    // Function below is very "temporarly" and should never be implemented this way i know that
    public static void clearScr() {
        for(int i =0;i<10;i++){
            System.out.println();
        }
    }

}


